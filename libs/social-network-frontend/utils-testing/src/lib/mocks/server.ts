import { setupServer } from 'msw/node';
import { graphql } from 'msw';
import { mockUser } from './mockUser';
import { environment } from '@sn-htc/social-network-frontend/config-env';

const handlers = [

];

export const server = setupServer(...handlers);
