

# Social Network

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Hieutran303_sn-htc&metric=alert_status)](https://sonarcloud.io/dashboard?id=Hieutran303_sn-htc)
[![Social Network](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/detailed/incdop&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/incdop/runs)
[![Hieutran303](https://circleci.com/bb/Hieutran303/sn-htc.svg?style=shield)](https://circleci.com/bb/Hieutran303/sn-htc)
[![This project is using Percy.io for visual regression testing.](https://percy.io/static/images/percy-badge.svg)](https://percy.io/a2d58f53/social-network-app)

This project was generated using [Nx](https://nx.dev).

## Serve

Run `nx serve my-app` to run the project.

## Build

Run `nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.
